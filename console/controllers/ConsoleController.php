<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 03.08.16
 * Time: 23:04
 */

namespace console\controllers;

use yii\console\Controller;
use common\models\FeedInterface;
use common\models\Event;
use common\models\Film;
use common\models\Music;

class ConsoleController extends Controller
{
    public function __construct($id,$module,$config = [])
    {
        parent::__construct($id,$module,$config);
        \Yii::$app->language = "ru-RU";
        date_default_timezone_set("Europe/Moscow");
    }


    // Команда "yii example/create test" вызовет "actionCreate('test')"
    public function actionImportFeed()
    {
        $feed_json = file_get_contents("console/import/import.json");
        $position_json = file_get_contents("console/import/position.json");

        $data = json_decode($feed_json);
        $records = $data->data;

        $position = json_decode($position_json);
        $i = $position->position;

        while($i<count($records))
        {
            $n = $i + 1;
            echo "Импортирую {$n}-ю запись..\r\n";

            $row = (array) $records[$i];

            $class_name = "common\\models\\".$row['type'];

            unset($row['type']);

            $entity = new $class_name();
            /* @var $entity FeedInterface */
            $entity->import_data($row);

            if ($entity->save())
            {
                echo "Успешно!\r\n";
            }
            else
            {
                echo "Ошибка\r\n";
                var_dump($entity->getErrors());
                break;
            }

            $i++;
            $position->position++;
            file_put_contents("console/import/position.json",json_encode($position));
            if ($i<count($records))
            {
                echo "Жду 10 секунд.... \r\n";
                sleep(10);
            }

        }

        echo "Импорт успешно завершен";
    }
}