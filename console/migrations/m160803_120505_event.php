<?php

use yii\db\Migration;

class m160803_120505_event extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'date' => $this->timestamp()->notNull(),
            'picture' => $this->string(),
            'description' => $this->string()->notNull(),
            'published' => $this->timestamp()->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
