<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%film}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $year
 * @property string $picture
 * @property string $description
 * @property string $published
 */
class Film extends FeedEntity implements FeedInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%film}}';
    }

    /**
     * Возвращаем тип записи в ленте - по сути, имя класса, за счет чего будем выстраивать ленту
     * @return string
     */
    public static function entity_type()
    {
        return "Film";
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'year', 'picture'], 'required'],
            [['published'], 'safe'],
            [['description', 'picture'], 'string'],
            [['year'], 'integer'],
            [['title', 'picture'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'year' => 'Год выпуска',
            'picture' => 'Обложка',
            'description' => 'Описание',
            'published' => 'Дата публикации',
        ];
    }

    /**
     * Контент записи - это описание события
     * @return string
     */
    public function feed_content()
    {
        return "<time>Год выпуска: {$this->year}</time><br/>".$this->description;
    }

    /**
     * Заголовок записи в ленте - это заголовок ивента
     * @return string
     */
    public function feed_title()
    {
        return $this->title;
    }

    /**
     * Картинка в ленте
     * @return string
     */
    public function feed_picture()
    {
        return $this->picture;
    }

    /**
     * Дата события для ленты
     * @return bool|string
     */
    public function feed_date()
    {
        return date("d.m.Y H:i",strtotime($this->published));
    }

    public function import_data($data)
    {
        parent::import_data($data);

        $file_local_name = "frontend/web/images/".pathinfo($this->picture,PATHINFO_BASENAME);
        file_put_contents($file_local_name,file_get_contents($this->picture));

        $this->picture = "/images/".pathinfo($this->picture,PATHINFO_BASENAME);
    }
}
