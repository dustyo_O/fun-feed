<?php

namespace common\models;

use Yii;
use common\models\Event;
use common\models\Film;
use common\models\Music;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%feed}}".
 *
 * @property integer $id
 * @property integer $entity_id
 * @property string $entity_class
 */
class Feed extends \yii\db\ActiveRecord
{
    const ENTITIES_PER_PAGE = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feed}}';
    }

    /**
     * Типы записей ленты
     * @return array
     */
    public static function types()
    {
        return [Film::entity_type(),Music::entity_type(),Event::entity_type()]; // массив всех существующих на данный момент типов
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id'], 'integer'],
            [['entity_class'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_id' => 'Entity ID',
            'entity_class' => 'Entity Class',
        ];
    }

    /**
     * Вспомогательная фукнция, получающая idшники требуемого типа
     *
     * @param $feed Feed[] Загруженное из таблицы feed - id записей, их тип и id в таблицах типов
     * @param $type string Тип, для которого надо найти id
     * @return int[] массив id
     */
    private static function getFeedTypeIds($feed,$type)
    {
        $result = [];
        foreach($feed as $entity)
        {
            if ($entity->entity_class === $type)
            {
                $result[] = $entity->entity_id;
            }
        }

        return $result;
    }

    /**
     * Вытягиваем дерево записей в одномерный массив вдоль данных ленты
     *
     * @param $feed Feed[] Загруженное из таблицы feed - id записей, их тип и id в таблицах типов
     * @param $tree FeedInterface[][] Массив-дерево записей, внешний ключ - тип, внутри просто целочисленные ключи
     * @return FeedInterface[] - массив записей с целочисленными ключами
     */
    private static function sortFeedsTree($feed, $tree)
    {
        $result = [];
        foreach($feed as $entity)
        {
            foreach($tree[$entity->entity_class] as $branch) // если id ветки равно entity_id в ленте - это правильная запись
            {
                if ($branch->id === $entity->entity_id)
                {
                    $result[] = $branch;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Функция для получения страницы ленты
     *
     * @param int $page Номер страницы
     * @param string[] $feed_filter Фильтр по типу записей
     * @param bool|string $date_filter Фильтр по дате
     * @return FeedInterface[] Записи со страницы
     */
    public static function getFeedPage($page = 1, $feed_filter = [], $date_filter = false)
    {
        if (!$feed_filter) $feed_filter = static::types();

        $feed = static::generateFeedQuery($feed_filter, $date_filter)->offset(($page-1)*static::ENTITIES_PER_PAGE)->limit(static::ENTITIES_PER_PAGE)->orderBy('id DESC')->all();

        $feeds_tree = []; // массив-дерево записей по разным типам
        foreach($feed_filter as $type)
        {

            $ids = static::getFeedTypeIds($feed,$type);
            if ($ids)
            {
                $class_name = "\\common\\models\\".$type;
                $feeds_tree[$type] = $class_name::find()->where(["id" => $ids])->all();
            }
        }

        $feed_page = static::sortFeedsTree($feed,$feeds_tree);

        return $feed_page;

    }

    /**
     * Получение количества страниц
     * @param string[] $feed_filter Фильтр по типу записей
     * @return int количество страниц
     */
    public static function getPageNum($feed_filter = [])
    {
        if (!$feed_filter) $feed_filter = static::types();

        $count = static::getRecordsCount($feed_filter);

        return ceil($count/static::ENTITIES_PER_PAGE);
    }

    /**
     * Получение количества записей согласно фильтру
     * @param string[] $feed_filter Фильтр по типу записей
     * @param bool|string $date_filter Фильтр по дате
     * @return int количество страниц
     */
    public static function getRecordsCount($feed_filter = [], $date_filter = false)
    {
        if (!$feed_filter) $feed_filter = static::types();

        $query = static::generateFeedQuery($feed_filter,$date_filter);

        $count = $query->count();

        return $count;
    }

    /**
     * SQL магия фильтрации творится здесь
     *
     * @param array $feed_filter Фильтр по типу записей
     * @param bool|string $date_filter Фильтр по дате
     * @return $this|bool|ActiveQuery
     */
    private static function generateFeedQuery($feed_filter = [], $date_filter = false)
    {

        if ($date_filter)
        {
            if (!$feed_filter) $feed_filter = static::types();
            $result = false;

            foreach ($feed_filter as $flt)
            {
                $join_query = Feed::find()->select(['feed.id','feed.entity_id','feed.entity_class']);
                $join_table = strtolower($flt);
                $join_query->join("RIGHT JOIN",$join_table,"`feed`.`entity_id` = `{$join_table}`.`id`");
                $join_query->where(['entity_class' => $flt]);

                $time_start = strtotime($date_filter);
                if ($time_start)
                {
                    $time_end = $time_start + 60*60*24; // +сутки

                    $date_start = date("Y-m-d", $time_start);
                    $date_end = date("Y-m-d", $time_end);

                    $join_query->andWhere(['between', "`{$join_table}`.`published`", $date_start, $date_end]);
                }


                if ($result === false)
                {
                    $result = $join_query;
                }
                else
                {
                    /* @var $result ActiveQuery */
                    $result->union($join_query);
                }
            }
        }
        else
        {
            $result = Feed::find()->select(['feed.id','feed.entity_id','feed.entity_class'])->where(['entity_class' => $feed_filter]);
        }

        return $result;
    }
}
