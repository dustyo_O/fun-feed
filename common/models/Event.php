<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $picture
 * @property string $description
 * @property string $published
 */
class Event extends FeedEntity implements FeedInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * Возвращаем тип записи в ленте - по сути, имя класса, за счет чего будем выстраивать ленту
     * @return string
     */
    public static function entity_type()
    {
        return "Event";
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'date', 'description'], 'required'],
            [['published','date'], 'safe'],
            [['title', 'picture', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'date' => 'Дата события',
            'picture' => 'Картинка',
            'description' => 'Описание',
            'published' => 'Дата публикации'
        ];
    }

    /**
     * Контент записи - это описание события
     * @return string
     */
    public function feed_content()
    {
        return $this->description;
    }

    /**
     * Заголовок записи в ленте - это заголовок ивента
     * @return string
     */
    public function feed_title()
    {
        return $this->title;
    }

    /**
     * Картинка в ленте
     * @return string
     */
    public function feed_picture()
    {
        return $this->picture;
    }

    /**
     * Дата события для ленты
     * @return bool|string
     */
    public function feed_date()
    {
        return date("d.m.Y H:i",strtotime($this->published));
    }

    public function import_data($data)
    {
        parent::import_data($data);
        $this->date = date("Y-m-d H:i",strtotime($this->date));

        $file_local_name = "frontend/web/images/".pathinfo($this->picture,PATHINFO_BASENAME);
        file_put_contents($file_local_name,file_get_contents($this->picture));

        $this->picture = "/images/".pathinfo($this->picture,PATHINFO_BASENAME);
    }
}
