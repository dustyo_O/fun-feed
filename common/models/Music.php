<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%music}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property string $published
 */
class Music extends FeedEntity implements FeedInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%music}}';
    }

    /**
     * Возвращаем тип записи в ленте - по сути, имя класса, за счет чего будем выстраивать ленту
     * @return string
     */
    public static function entity_type()
    {
        return "Music";
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link'], 'required'],
            [['published'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'link' => 'Ссылка',
            'published' => 'Дата публикации',
        ];
    }

    /**
     * Контент записи - это ссылка на файл
     * @return string
     */
    public function feed_content()
    {
        return <<<HTML
<a href="/music/{$this->link}">{$this->title}</a>
HTML;

    }

    /**
     * Заголовок записи в ленте - это заголовок ивента
     * @return string
     */
    public function feed_title()
    {
        return $this->title;
    }

    /**
     * Картинка в ленте - для музыки это пустота
     * @return string
     */
    public function feed_picture()
    {
        return "https://placeholdit.imgix.net/~text?txtsize=33&txt=No%20Image&w=150&h=150";
    }

    /**
     * Дата публикации для ленты
     * @return bool|string
     */
    public function feed_date()
    {
        return date("d.m.Y H:i",strtotime($this->published));
    }

    public function import_data($data)
    {
        parent::import_data($data);

        $file_local_name = "frontend/web/music/".pathinfo($this->link,PATHINFO_BASENAME);
        file_put_contents($file_local_name,file_get_contents($this->link));

        $this->link = $file_local_name;
    }
}
