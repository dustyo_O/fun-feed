<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 03.08.16
 * Time: 13:51
 */

namespace common\models;

interface FeedInterface
{
    public function import_data($data);
    public function save_feed();
    public static function entity_type();

    public function feed_picture();
    public function feed_date();
    public function feed_content();
    public function feed_title();
}