<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 03.08.16
 * Time: 15:17
 */
namespace common\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * Class FeedEntity
 * Абстрактный базовый класс, описывающий общее поведение для всех сущностей, которые принимают участие в ленте
 *
 * @package common\models
 */
abstract class FeedEntity extends ActiveRecord implements FeedInterface
{
    /**
     * Импорт данных, раскодированных в json в виде ассоциативного массива ключ-значение
     * @param $data
     */
    public function import_data($data)
    {
        foreach ($data as $field => $value)
        {
            if (in_array($field, array_keys($this->attributeLabels()))) // если не перечислен в полях, то пропускаем данные (ошибка в файле?)
            {
                $this->$field = $value;
            }

        }
    }

    /**
     * Сохранение единицы в ленту
     */
    public function save_feed()
    {
        if ($this->id !== NULL)
        {
            $feed = Feed::find()->where(["entity_id" => $this->id, "entity_class" => $this->entity_type()])->one();

            if ($feed)
            {
                return true; // элемент уже есть в ленте
            }
            else
            {
                $feed = new Feed();
                $feed->entity_id = $this->id;
                $feed->entity_class = static::entity_type();

                return $feed->save();
            }
        }
        else
        {
            return false; // нельзя назвать успешным сохранение, если сохранить в ленту нельзя
        }
    }

    /**
     * Стандартное событие после сохранения записи - надо создать отметку в ленте.
     *
     * @param bool $insert флаг о том, является ли операция вставкой новой строки
     * @param array $attributes измененные атрибуты
     *
     */
    public function afterSave($insert, $attributes)
    {
        if ($insert) // дергать ленту есть смысл только при первичном сохранении
        {
            $this->save_feed();

        }

        parent::afterSave($insert, $attributes);
    }

    /**
     * Событие при сохранении записи - устанавливаем дату публикации
     *
     * @param bool $insert
     * @return bool флаг о том, что все ок и можно сохранять
     */
    public function beforeSave($insert)
    {
        if ($insert)
        {
            $this->published = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
}