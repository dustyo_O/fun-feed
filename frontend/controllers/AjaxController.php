<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 04.08.16
 * Time: 1:05
 */

namespace frontend\controllers;

use common\models\Feed;
use Yii;
use yii\web\Controller;
use common\models\FeedInterface;


/**
 * Ajax controller
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Получение записей для страницы в json формате (+ постраничность)
     */
    public function actionFeed()
    {
        $filter = Yii::$app->request->post("filter",NULL);
        $date_filter = Yii::$app->request->post("date",false);
        $page = Yii::$app->request->post("page",NULL);

        if ($filter !== NULL)
        {
            $feed = Feed::getFeedPage($page, $filter,$date_filter);
            $pages = Feed::getPageNum($filter,$date_filter);
        }
        else
        {
            $feed = Feed::getFeedPage($page,[],$date_filter);
            $pages = Feed::getPageNum([],$date_filter);
        }

        /* @var $feed FeedInterface[] */
        if (count($feed))
        {
            $result = [];
            foreach ($feed as $entity)
            {
                $row = [];
                $row['title'] = $entity->feed_title();
                $row['content'] = $entity->feed_content();
                $row['picture'] = $entity->feed_picture();
                $row['date'] = $entity->feed_date();

                $result[] = $row;
            }

            $feed_json = json_encode($result);

            return <<<JSON
{"status": "success", "feed": {$feed_json}, "pages": {$pages}}
JSON;

        }
        else
        {
            return <<<JSON
{"status": "error", "msg": "Нет записей"}
JSON;

        }

    }

    /**
     * Выдача статистики
     */
    public function actionStat()
    {
        $films_all_time_stat = Feed::getRecordsCount(["Film"]);
        $music_all_time_stat = Feed::getRecordsCount(["Music"]);
        $events_all_time_stat = Feed::getRecordsCount(["Event"]);

        $all_time_stat = Feed::getRecordsCount();

        $today = date("Y-m-d");

        $films_today_stat = Feed::getRecordsCount(["Film"],$today);
        $music_today_stat = Feed::getRecordsCount(["Music"],$today);
        $events_today_stat = Feed::getRecordsCount(["Event"],$today);

        $today_stat = Feed::getRecordsCount([],$today);


        return <<<JSON
{"status": "success", "stat":[
    {"title": "Сегодня фильмов", "content": "{$films_today_stat}"},
    {"title": "Сегодня музыки", "content": "{$music_today_stat}"},
    {"title": "Сегодня событий", "content": "{$events_today_stat}"},
    {"title": "Сегодня записей", "content": "{$today_stat}"},
    {"title": "Всего фильмов", "content": "{$films_all_time_stat}"},
    {"title": "Всего музыки", "content": "{$music_all_time_stat}"},
    {"title": "Всего событий", "content": "{$events_all_time_stat}"},
    {"title": "Всего записей", "content": "{$all_time_stat}"}
]}
JSON;


    }


}