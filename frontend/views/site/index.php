<?php

/* @var $this yii\web\View */

use app\assets\DatePickerAsset;

$this->title = 'Тестовое задание';

$this->registerJs(
    <<<JS
    //запрос и обновление ленты
    var page = 1;

    function requestFeed()
    {
        $.ajax(
            {
                url: "/ajax/feed",
                type: "POST",
                data: {page: page, filter: getFilter(), date: $("#date-filter").val()},
                dataType: "json"
            }
        ).done(
            // обработка сообщения-ответа сервера
            function (msg)
            {
                $("#feed").empty();
                $("#pagination").empty();
                if (msg.status === 'success')
                {
                    $.each(msg.feed, function(i,item){
                        $("#feed")
                        .append(
                            $("<div/>",{class: "row"}).append(
                                $("<div/>", {class: "col-md-4"}).append(
                                    $("<img/>",{src: item.picture, class: "img-responsive"})
                                )

                            ).append(
                                $("<div/>", {class: "col-md-8"}).append(

                                    $("<h3/>",{text: item.title})
                                )
                                .append(
                                    $("<p/>").html(item.content)
                                )

                            )

                        )
                    });

                    var paginator = $("<ul/>",{class: "pagination"});

                    for(var i = 1; i <= msg.pages; i++)
                    {
                        var page_link = $("<a/>",{href: "#", "data-page": i});

                        if (i === page)
                        {
                            page_link.append(
                                $("<strong/>", {text: i})
                            );
                        }
                        else
                        {
                            page_link.text(i);
                        }

                        page_link.click(
                            function(e)
                            {
                                e.preventDefault();
                                page = $(this).data("page");
                                requestFeed();
                            }
                        );

                        paginator.append(
                            $("<li/>").append(page_link)
                        );

                    }

                    $("#pagination").append(
                        $("<div/>",{class: "col-sm-12"})
                    ).append(paginator);
                }
                else
                {
                    $("#feed").append(

                        $("<div/>", {class: "alert alert-danger", role: "alert", text: msg.msg})
                    );

                }

            }
        )
        .fail(
            // вывод ошибки
            function()
            {
                $("#feed").empty();
                $("#feed").append(

                    $("<div/>", {class: "alert alert-danger", role: "alert", text: "Ошибка получения данных"})
                );

            }
        );
    }

    // получение фильтра
    function getFilter()
    {
        var result = [];
        $("#filter input:checked").each(
            function()
            {
                result[result.length] = $(this).data('filter'); // получаем фильтр из дата-атрибута
            }
        );

        return result;
    }


    // одинаковые обработчики смены фильтра
    $("#filter [data-filter]").change(
        function()
        {
            page = 1;
            requestFeed();
        }
    );

    $("#date-filter").change(
        function()
        {
            page = 1;
            requestFeed();
        }
    );

    // обработка клика по статистике
    $("a[href='/site/stat']").click(
        function(event)
        {
            event.preventDefault();

            $("#statModal .modal-body").empty();

            $.ajax(
                {
                    url: "/ajax/stat",
                    type: "POST",
                    data: {},
                    dataType: "json"
                }
            ).done(
                // обработка сообщения-ответа сервера
                function (msg)
                {
                    if (msg.status === 'success')
                    {
                        // собираем таблицу данных статистики
                        var table = $("<table/>",{class:"table"});
                        $.each(msg.stat, function(i, item){
                            table.append( // данные - это массив из пар title + content
                                $("<tr/>").append(
                                    $("<td/>",{text:item.title})
                                ).append(
                                    $("<td/>",{text:item.content})
                                )
                            )
                        });
                        $("#statModal .modal-body").append(
                            table
                        );
                    }
                    else
                    {
                        $("#statModal .modal-body").append(
                            $("<div/>", {class: "alert alert-danger", role: "alert", text: msg.msg})
                        );

                    }
                }
            ).fail(
                function ()
                {
                    $("#statModal .modal-body").append(
                        $("<div/>", {class: "alert alert-danger", role: "alert", text: "Ошибка получения данных"})
                    );
                }
            ).always(
                function()
                {
                    $("#statModal").modal();
                }
            );


        }
    );

    $("#date-filter").datepicker({clearBtn: true,language:'ru', endDate:"now"});

    //requestFeed();
JS
);

DatePickerAsset::register($this);
?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12 col-lg-offset-3 col-md-offset-2">
                <h2>Лента</h2>
                <div id="filter" class="row">
                    <div class="col-sm-12 form-group">
                        <label for="film">Фильмы</label><input id="film" type="checkbox" data-filter="Film"/>
                        <label for="music">Музыка</label><input id="music" type="checkbox" data-filter="Music"/>
                        <label for="event">События</label><input id="event" type="checkbox" data-filter="Event"/>
                        <div class="form-inline pull-right">
                            <input id="date-filter" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div id="feed" class="row">

                </div>
                <div id="pagination" class="row">

                </div>

            </div>

        </div>

    </div>
</div>

<!-- Modal -->
<div id="statModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Статистика</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>
