<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 04.08.16
 * Time: 12:33
 */

namespace app\assets;

use yii\web\AssetBundle;

class DatePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-datepicker/dist';

    public $css = [
        'css/bootstrap-datepicker.min.css',
    ];

    public $js = [
        'js/bootstrap-datepicker.min.js',
        'locales/bootstrap-datepicker.ru.min.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}